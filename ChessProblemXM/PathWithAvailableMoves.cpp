#include "stdafx.h"
#include "PathWithAvailableMoves.h"

PathWithAvailableMoves::PathWithAvailableMoves(const string& newPath, const vector<shared_ptr<Position>>&  newMoves, const unsigned int d)
	: currentPath(newPath)
	, availableMoves(newMoves)
	, depth(d)
{
}


const vector<shared_ptr<Position> >& PathWithAvailableMoves::getAvailableMoves() const
{
	return availableMoves;
}

const string& PathWithAvailableMoves::getCurrentPath() const
{
	return currentPath;
}

const unsigned int PathWithAvailableMoves::getDepth() const
{
	return depth;
}

PathWithAvailableMoves::~PathWithAvailableMoves()
{
}
