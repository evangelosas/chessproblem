#pragma once
#include "Board.h"
#include <array>

using namespace std;

class StandardBoard :
	public Board
{
public:
	StandardBoard();
	StandardBoard(const StandardBoard& that) = delete;
	StandardBoard& operator=(const StandardBoard& that) = delete;
	virtual optional<shared_ptr<Position>> getPosition(int column, int row) override;

	static const unsigned int getPositionIndex(int column, int row);

	~StandardBoard();

private:
	array< array< shared_ptr<Position>, Board::STANDARD_LENGTH>, Board::STANDARD_LENGTH> standardPositions;
	
};

