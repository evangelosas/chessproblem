#include "stdafx.h"
#include "DFSAlgorithm.h"
#include "Piece.h"
#include "Position.h"
#include "StandardBoard.h"

DFSAlgorithm::DFSAlgorithm()
{
}

string DFSAlgorithm::solve(shared_ptr<Piece>& piece, const shared_ptr<Position>& ending, unsigned int depth)
{
	vpl.reset();
	if (piece->getCurrentPosition()->equals(ending))
	{
		return ending->getName();
	}
	string result = recursiveSolve(piece, ending, depth);
	if (result == "") return "Not possible";
	else return result;

}

// Recursive DFS
string DFSAlgorithm::recursiveSolve(shared_ptr<Piece>& piece, const shared_ptr<Position>& ending, unsigned int depth)
{
	if (depth == -1) return "";
	if (piece->getCurrentPosition()->equals(ending))
	{
		return ending->getName();
	}
	auto startPos = piece->getCurrentPosition();
	vpl.set(StandardBoard::getPositionIndex(piece->getCurrentPosition()->getColumn(), piece->getCurrentPosition()->getRow()));
	auto availableMoves = piece->calculateAvailableMoves();
	for (auto currentPosition : availableMoves)
	{
		auto index = StandardBoard::getPositionIndex(currentPosition->getColumn(), currentPosition->getRow());
		if (!vpl.get(index))
		{
			
			piece->setNewPosition(currentPosition);
			auto result = recursiveSolve(piece, ending, depth - 1);
			if (result != "")
			{
				return startPos->getName() + NEXT + result;
			}
		}
	}
	return "";
}

DFSAlgorithm::~DFSAlgorithm()
{
}
