#pragma once
#include "Board.h"
#include <bitset>

using namespace std;

class VisitedItemList
{
public:
	VisitedItemList();
	VisitedItemList(const VisitedItemList& that) = delete;
	VisitedItemList& operator=(const VisitedItemList& that) = delete;
	virtual ~VisitedItemList();
	bool get(const int index) const;
	void set(const int index);
	void reset();
	static const unsigned int SIZE = Board::STANDARD_LENGTH * Board::STANDARD_LENGTH;
private:
	bitset<SIZE> list;
};

