#include "stdafx.h"
#include "BFSAlgorithm.h"
#include "StandardPosition.h"
#include "Piece.h"
#include "StandardBoard.h"

using namespace std;

BFSAlgorithm::BFSAlgorithm()
{
	queuePositions = make_unique<queue<PathWithAvailableMoves>>();
}

// An implementation of the BFS Algorithm
string BFSAlgorithm::solve(shared_ptr<Piece>& piece, const shared_ptr<Position>& ending, unsigned int depth)
{
	vpl.reset();
	emptyQueuePositions();
	string result = piece->getCurrentPosition()->getName();
	if (piece->getCurrentPosition()->equals(ending))
	{
		return result;
	}
	queuePositions->emplace(PathWithAvailableMoves(result, piece->calculateAvailableMoves(),0));
	vpl.set(StandardBoard::getPositionIndex(piece->getCurrentPosition()->getColumn(), piece->getCurrentPosition()->getRow()));
	
	while (!queuePositions->empty())
	{
		auto availablePaths = queuePositions->front();
		for (auto currentPosition : availablePaths.getAvailableMoves())
		{
			auto index = StandardBoard::getPositionIndex(currentPosition->getColumn(), currentPosition->getRow());
			if (!vpl.get(index))
			{
				auto currentDepth = availablePaths.getDepth();
				if (currentPosition->equals(ending) && currentDepth < depth)
				{
					return availablePaths.getCurrentPath() + NEXT + currentPosition->getName();
				}
				vpl.set(index);
				if (currentDepth < depth)
				{
					piece->setNewPosition(currentPosition);
					queuePositions->emplace(
						PathWithAvailableMoves(
							availablePaths.getCurrentPath() + NEXT + currentPosition->getName(),
							piece->calculateAvailableMoves(),
							++currentDepth
						)
					);
				}
			}
		}
		queuePositions->pop();
	}
	return "Not possible";
}

void BFSAlgorithm::emptyQueuePositions()
{
	while (!queuePositions->empty()) queuePositions->pop();
}


BFSAlgorithm::~BFSAlgorithm()
{
}

