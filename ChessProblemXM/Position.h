#pragma once
#include <string>
#include <memory>

using namespace std;

class Position 
{

public:
	virtual int getRow() const = 0;
	virtual int getColumn() const = 0;
	virtual const string getName() const = 0;

	friend bool operator==(const Position& lhs, const Position& rhs);
	friend bool operator!=(const Position& lhs, const Position& rhs);

	virtual bool equals(const shared_ptr<Position>& rhs) const;

	virtual ~Position() {}
};