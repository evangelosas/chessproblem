#pragma once
#include "Position.h"
#include <optional>

class StandardPosition :
	public Position
{

public:
	virtual ~StandardPosition();
	virtual int getRow() const override;
	virtual int getColumn() const override;
	virtual const string getName() const override;
		
	static optional<shared_ptr<Position>> generateFromChessPosition(const char c, const int r);
private:
	StandardPosition(int c, int r);
	int column;
	int row;
};
