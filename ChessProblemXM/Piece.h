#pragma once
#include <vector>
#include <memory>

using namespace std;

class Position;
class Board;

class Piece
{
public:
	virtual vector<shared_ptr<Position>> calculateAvailableMoves() = 0;
	virtual void setNewPosition(const shared_ptr<Position>& pos) = 0;
	virtual shared_ptr<Position>& getCurrentPosition() = 0;
	virtual ~Piece() {}

protected:
	shared_ptr<Position> currentPosition;
	shared_ptr<Board> board;

};

