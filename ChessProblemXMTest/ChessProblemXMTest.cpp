#include "stdafx.h"
#include "CppUnitTest.h"
#include "../ChessProblemXM/BFSAlgorithm.h"
#include "../ChessProblemXM/DFSAlgorithm.h"
#include "../ChessProblemXM/StandardPosition.h"
#include "../ChessProblemXM/StandardBoard.h"
#include "../ChessProblemXM/Board.h"
#include "../ChessProblemXM/Knight.h"
#include <string>
#include <memory>
#include <optional>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ChessProblemXMTest
{
	TEST_CLASS(StandardPositionTest)
	{
	public:

		TEST_METHOD(getName)
		{
			//Normal Case
			shared_ptr<Position> p1 = StandardPosition::generateFromChessPosition('A', 8).value();
			string result1 = p1->getName();
			Assert::AreEqual("A8", result1.c_str());

			//Small letter
			shared_ptr<Position> p2 = StandardPosition::generateFromChessPosition('h', 8).value();
			string result2 = p2->getName();
			Assert::AreEqual("H8", result2.c_str());

			//Illegal column
			bool result3 = StandardPosition::generateFromChessPosition('z', 8).has_value();
			Assert::AreEqual(false, result3);

			//Illegal row
			bool result31 = StandardPosition::generateFromChessPosition('h', 9).has_value();
			Assert::AreEqual(false, result31);
		}

	};

	TEST_CLASS(StandardBoardTest)
	{
	public:

		TEST_METHOD(ConstructorAndGet)
		{
			StandardBoard sb;

			//Lower left corner
			shared_ptr<Position> p1 = sb.getPosition(Board::STANDARD_LENGTH - 1, Board::STANDARD_LENGTH - 1).value();
			string result1 = p1->getName();
			Assert::AreEqual("H8", result1.c_str());

			//Illegal position
			bool result2 = sb.getPosition(Board::STANDARD_LENGTH, Board::STANDARD_LENGTH).has_value();
			Assert::AreEqual(false, result2);
		}

	};

	TEST_CLASS(KnightTest)
	{
	public:

		TEST_METHOD(AvailableMoves)
		{
			shared_ptr<StandardBoard> sb = make_shared<StandardBoard>();
			shared_ptr<Piece> knight = Knight::generateKnightInSpecifiedPosition(sb, 1000, 1000);

			//Test 1 - Default Position (Centre of the board)
			auto currentPosition = knight->getCurrentPosition();
			string result0 = currentPosition->getName();
			Assert::AreEqual("E5", result0.c_str());
			auto moves = knight->calculateAvailableMoves();
			Assert::AreEqual((size_t)8, moves.size());
			string result1 = moves[0]->getName();
			Assert::AreEqual("C4", result1.c_str());
			string result2 = moves[1]->getName();
			Assert::AreEqual("C6", result2.c_str());
			string result3 = moves[2]->getName();
			Assert::AreEqual("D7", result3.c_str());
			string result4 = moves[3]->getName();
			Assert::AreEqual("F7", result4.c_str());
			string result5 = moves[4]->getName();
			Assert::AreEqual("G6", result5.c_str());
			string result6 = moves[5]->getName();
			Assert::AreEqual("G4", result6.c_str());
			string result7 = moves[6]->getName();
			Assert::AreEqual("F3", result7.c_str());
			string result8 = moves[7]->getName();
			Assert::AreEqual("D3", result8.c_str());

			//Test 2 - Row 8 (E8)
			knight->setNewPosition(sb->getPosition(4, Board::STANDARD_LENGTH - 1).value());
			currentPosition = knight->getCurrentPosition();
			string result20 = currentPosition->getName();
			Assert::AreEqual("E8", result20.c_str());
			auto moves2 = knight->calculateAvailableMoves();
			Assert::AreEqual((size_t)4, moves2.size());
			string result21 = moves2[0]->getName();
			Assert::AreEqual("C7", result21.c_str());
			string result22 = moves2[1]->getName();
			Assert::AreEqual("G7", result22.c_str());
			string result23 = moves2[2]->getName();
			Assert::AreEqual("F6", result23.c_str());
			string result24 = moves2[3]->getName();
			Assert::AreEqual("D6", result24.c_str());

			//Test 3 - A1
			knight->setNewPosition(sb->getPosition(0, 0).value());
			currentPosition = knight->getCurrentPosition();
			string result30 = currentPosition->getName();
			Assert::AreEqual("A1", result30.c_str());
			auto moves3 = knight->calculateAvailableMoves();
			Assert::AreEqual((size_t)2, moves3.size());
			string result31 = moves3[0]->getName();
			Assert::AreEqual("B3", result31.c_str());
			string result32 = moves3[1]->getName();
			Assert::AreEqual("C2", result32.c_str());
		}

	};


	TEST_CLASS(BFSAlgorithmTest)
	{
	public:

		TEST_METHOD(BFSsolve)
		{
			BFSAlgorithm bfsalgorithm;
			shared_ptr<StandardBoard> sb = make_shared<StandardBoard>();
			shared_ptr<Piece> knight = Knight::generateKnightInSpecifiedPosition(sb, StandardPosition::generateFromChessPosition('A', 8).value());
			
			//Test 1 : Same square
			shared_ptr<Position> endPos = StandardPosition::generateFromChessPosition('A', 8).value();
			string result = bfsalgorithm.solve(knight, endPos, 3);
			Assert::AreEqual("A8", result.c_str());

			//Test 2a : Unique case one step case
			knight->setNewPosition(StandardPosition::generateFromChessPosition('E', 5).value());
			shared_ptr<Position> endPos2a = StandardPosition::generateFromChessPosition('C', 4).value();
			string result2a = bfsalgorithm.solve(knight, endPos2a, 3);
			Assert::AreEqual("E5-->C4", result2a.c_str());

			//Test 2b : Unique case again to compare with dfs
			knight->setNewPosition(StandardPosition::generateFromChessPosition('E', 5).value());
			shared_ptr<Position> endPos2b = StandardPosition::generateFromChessPosition('C', 6).value();
			string result2b = bfsalgorithm.solve(knight, endPos2b, 3);
			Assert::AreEqual("E5-->C6", result2b.c_str());
			
			//Test 3 : No solution
			knight->setNewPosition(StandardPosition::generateFromChessPosition('E', 5).value());
			shared_ptr<Position> endPos3 = StandardPosition::generateFromChessPosition('C', 3).value();
			string result3 = bfsalgorithm.solve(knight, endPos3, 3);
			Assert::AreEqual("Not possible", result3.c_str());
			
			//Test 4 : Unique three step case
			knight->setNewPosition(StandardPosition::generateFromChessPosition('A', 1).value());
			shared_ptr<Position> endPos4 = StandardPosition::generateFromChessPosition('D', 7).value();
			string result4 = bfsalgorithm.solve(knight, endPos4, 3);
			Assert::AreEqual("A1-->B3-->C5-->D7", result4.c_str());
			
			//Test 5 : More than one solution two steps
			knight->setNewPosition(StandardPosition::generateFromChessPosition('E', 5).value());
			shared_ptr<Position> endPos5 = StandardPosition::generateFromChessPosition('D', 6).value();
			string result5 = bfsalgorithm.solve(knight, endPos5, 3);
			Assert::AreEqual("E5-->C4-->D6", result5.c_str());
		
		}

		

	};

	TEST_CLASS(DFSAlgorithmTest)
	{
	public:

		TEST_METHOD(DFSsolve)
		{
			DFSAlgorithm dfsalgorithm;
			shared_ptr<StandardBoard> sb = make_shared<StandardBoard>();
			shared_ptr<Piece> knight = Knight::generateKnightInSpecifiedPosition(sb, StandardPosition::generateFromChessPosition('A', 8).value());

			//Test 1 : Same square
			shared_ptr<Position> endPos = StandardPosition::generateFromChessPosition('A', 8).value();
			string result = dfsalgorithm.solve(knight, endPos, 3);
			Assert::AreEqual("A8", result.c_str());

			//Test 2a : Unique case one step case
			knight->setNewPosition(StandardPosition::generateFromChessPosition('E', 5).value());
			shared_ptr<Position> endPos2a = StandardPosition::generateFromChessPosition('C', 4).value();
			string result2a = dfsalgorithm.solve(knight, endPos2a, 3);
			Assert::AreEqual("E5-->C4", result2a.c_str());

			//Test 2b : Unique case but dfs does not take the shortest path
			knight->setNewPosition(StandardPosition::generateFromChessPosition('E', 5).value());
			shared_ptr<Position> endPos2b = StandardPosition::generateFromChessPosition('C', 6).value();
			string result2b = dfsalgorithm.solve(knight, endPos2b, 3);
			Assert::AreEqual("E5-->C4-->A5-->C6", result2b.c_str());

			//Test 3 : No solution
			knight->setNewPosition(StandardPosition::generateFromChessPosition('E', 5).value());
			shared_ptr<Position> endPos3 = StandardPosition::generateFromChessPosition('C', 3).value();
			string result3 = dfsalgorithm.solve(knight, endPos3, 3);
			Assert::AreEqual("Not possible", result3.c_str());

			////Test 4 : Unique three step case
			knight->setNewPosition(StandardPosition::generateFromChessPosition('A', 1).value());
			shared_ptr<Position> endPos4 = StandardPosition::generateFromChessPosition('D', 7).value();
			string result4 = dfsalgorithm.solve(knight, endPos4, 3);
			Assert::AreEqual("A1-->B3-->C5-->D7", result4.c_str());

			//Test 5 : More than one solution two steps
			knight->setNewPosition(StandardPosition::generateFromChessPosition('E', 5).value());
			shared_ptr<Position> endPos5 = StandardPosition::generateFromChessPosition('D', 6).value();
			string result5 = dfsalgorithm.solve(knight, endPos5, 3);
			Assert::AreEqual("E5-->C4-->D6", result5.c_str());

		}



	};
}