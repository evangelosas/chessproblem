#include "stdafx.h"
#include "VisitedItemList.h"


VisitedItemList::VisitedItemList()
{
}


VisitedItemList::~VisitedItemList()
{
}

bool VisitedItemList::get(const int index) const
{
	if (index >= 0 && index < SIZE)
	{
		return list[index];
	}
	return false;
}

void VisitedItemList::set(const int index)
{
	if (index >= 0 && index < SIZE)
	{
		list[index] = true;
	}
}

void VisitedItemList::reset()
{
	list.reset();
}
