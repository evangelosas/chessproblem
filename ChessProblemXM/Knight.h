#pragma once
#include "Piece.h"
#include <vector>

class Position;

class Knight :
	public Piece
{
public:
	Knight(const Knight& that) = delete;
	Knight& operator=(const Knight& that) = delete;
	virtual vector<shared_ptr<Position>> calculateAvailableMoves() override;
	virtual void setNewPosition(const shared_ptr<Position>& pos)  override;
	virtual shared_ptr<Position>& getCurrentPosition() override;
	virtual ~Knight();

	static shared_ptr<Piece> generateKnightInSpecifiedPosition(const shared_ptr<Board>& b, const shared_ptr<Position>& pos);
	static shared_ptr<Piece> generateKnightInSpecifiedPosition(const shared_ptr<Board>& b,const int column,const int row);
	static shared_ptr<Piece> generateKnightInDefaultPosition(const shared_ptr<Board>& b);
private:
	Knight(const shared_ptr<Board>& b);
};

