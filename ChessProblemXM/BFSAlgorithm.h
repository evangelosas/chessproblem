#pragma once
#include "Algorithm.h"
#include "VisitedItemList.h"
#include "PathWithAvailableMoves.h"
#include <string>
#include <queue>
#include <memory>

using namespace std;

class StandardPosition;

class BFSAlgorithm :
	public Algorithm
{
public:
	BFSAlgorithm();
	BFSAlgorithm(const BFSAlgorithm& that) = delete;
	BFSAlgorithm& operator=(const BFSAlgorithm& that) = delete;
	virtual string solve(shared_ptr<Piece>& piece, const shared_ptr<Position>& ending, unsigned int depth) override;
	virtual ~BFSAlgorithm();

protected:
	void emptyQueuePositions();

private:
	VisitedItemList vpl;
	unique_ptr<queue<PathWithAvailableMoves> > queuePositions;
};

