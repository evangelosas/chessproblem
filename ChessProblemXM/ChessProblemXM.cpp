// ChessProblemXM.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Algorithm.h"
#include "BFSAlgorithm.h"
#include "StandardPosition.h"
#include "StandardBoard.h"
#include "Knight.h"
#include "Piece.h"
#include <string>
#include <memory>
#include <iostream>
#include <cstdlib>

using namespace std;

bool illegal(char column, int row)
{
	return column < 'A' || column > 'H' || column < 'a' || column > 'h' || row < 1 || row > 8;
}

int main()
{
	try
	{
		char columnKnight, columnEnding;
		int rowKnight, rowEnding;
		cout << "Please give column for knight." << endl;
		cin >> columnKnight;
		cout << "Please give row for knight." << endl;
		cin >> rowKnight;
		if (illegal(columnKnight,rowKnight)) cout << "Switching to default position E5"<<endl;
		cout << "Please give column for ending StandardPosition." << endl;
		cin >> columnEnding;
		cout << "Please give row for ending StandardPosition." << endl;
		cin >> rowEnding;
		if (illegal(columnEnding,rowEnding))
		{
			cout << "Switching to A1"<<endl;
			columnEnding = 'A';
			rowEnding = '1';
		}

		unique_ptr<Algorithm> algorithm = make_unique<BFSAlgorithm>();
		shared_ptr<Board> board = make_shared<StandardBoard>();
		shared_ptr<Piece> knight = Knight::generateKnightInSpecifiedPosition(board, StandardPosition::generateFromChessPosition(columnKnight, rowKnight).value());
		cout << algorithm->solve(knight, StandardPosition::generateFromChessPosition(columnEnding, rowEnding).value(), 3) << endl;
	}
	catch (exception&) {
		cout << "Invalid input. Program terminates" << endl;
	}
	system("pause");
    return 0;
}

