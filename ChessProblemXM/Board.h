#pragma once

#include "Position.h"
#include <memory>
#include <optional>

class Board
{
public:

	virtual optional<shared_ptr<Position> > getPosition(int column, int row) = 0;
	virtual ~Board() {}

	static const unsigned int STANDARD_LENGTH = 8;
};

