#pragma once
#include <string>
#include <memory>

class Position;
class Piece;

using namespace std;

class Algorithm
{
public:
	virtual string solve(shared_ptr<Piece>& piece, const shared_ptr<Position>& ending, unsigned int depth) = 0;
	inline const static string NEXT = "-->";
	virtual ~Algorithm() {}
};

