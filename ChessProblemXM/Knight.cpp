#include "stdafx.h"
#include "Knight.h"
#include "StandardPosition.h"
#include "Board.h"

Knight::Knight(const shared_ptr<Board>& b)
{
	board = b;
}

void Knight::setNewPosition(const shared_ptr<Position>& pos)
{
	currentPosition = pos;
}

shared_ptr<Position>& Knight::getCurrentPosition()
{
	return currentPosition;
}

// Returns all the available moves that a knight can do from its current position
// starting from the lower left position and moving clockwise.
vector<shared_ptr<Position>> Knight::calculateAvailableMoves()
{
	vector<shared_ptr<Position>> availableMoves;
	availableMoves.reserve(8);
	int column = currentPosition->getColumn();
	int row = currentPosition->getRow();

	if (column >= 2)
	{
		if (row != 0)  
			availableMoves.emplace_back(board->getPosition(column - 2, row - 1).value());
		if (row != Board::STANDARD_LENGTH - 1)
			availableMoves.emplace_back(board->getPosition(column - 2, row + 1).value());

	}
	if (row <= Board::STANDARD_LENGTH - 3)
	{
		if (column != 0)  
			availableMoves.emplace_back(board->getPosition(column - 1, row + 2).value());
		if (column != Board::STANDARD_LENGTH - 1)
			availableMoves.emplace_back(board->getPosition(column + 1, row + 2).value());

	}
	if (column <= Board::STANDARD_LENGTH - 3)
	{
		if (row != Board::STANDARD_LENGTH - 1)
			availableMoves.emplace_back(board->getPosition(column + 2, row + 1).value());
		if (row != 0)
			availableMoves.emplace_back(board->getPosition(column + 2, row - 1).value());

	}
	if (row >= 2)
	{
		if (column != Board::STANDARD_LENGTH - 1)
			availableMoves.emplace_back(board->getPosition(column + 1, row - 2).value());
		if (column != 0)
			availableMoves.emplace_back(board->getPosition(column - 1, row - 2).value());

	}
	return availableMoves;
}


Knight::~Knight()
{
}

shared_ptr<Piece> Knight::generateKnightInSpecifiedPosition(const shared_ptr<Board>& b, const shared_ptr<Position>& pos)
{
	return generateKnightInSpecifiedPosition(b, pos->getColumn(), pos->getRow());
}

shared_ptr<Piece> Knight::generateKnightInSpecifiedPosition(const shared_ptr<Board>& b,const int column,const int row)
{
	auto check = b->getPosition(column, row);
	if (check.has_value())
	{
		shared_ptr<Knight> p(new Knight(b));
		p->currentPosition = check.value();
		return p;
	}
	else return generateKnightInDefaultPosition(b);

}

shared_ptr<Piece> Knight::generateKnightInDefaultPosition(const shared_ptr<Board>& b)
{
	shared_ptr<Knight> p(new Knight(b));
	p->currentPosition = p->board->getPosition(4, 4).value();
	return p;
}


