#pragma once

#include "Position.h"
#include <vector>
#include <string>

using namespace std;

class PathWithAvailableMoves
{
public:
	PathWithAvailableMoves(const string& newPath, const vector<shared_ptr<Position>>& newMoves, const unsigned int d);
	PathWithAvailableMoves& operator=(const PathWithAvailableMoves& that) = delete;
	const vector<shared_ptr<Position> >&  getAvailableMoves() const;
	const string& getCurrentPath() const;
	const unsigned int getDepth() const;
	~PathWithAvailableMoves();

private:
	const vector<shared_ptr<Position>>  availableMoves;
	const string currentPath;
	const unsigned int depth;
};

