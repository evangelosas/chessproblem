#include "stdafx.h"
#include "Position.h"

bool operator==(const Position& lhs, const Position& rhs)
{
	return lhs.getRow() == rhs.getRow() && lhs.getColumn() == rhs.getColumn();
}

bool operator!=(const Position & lhs, const Position & rhs)
{
	return !operator==(lhs, rhs);
}

bool Position::equals(const shared_ptr<Position>& rhs) const
{
	return (*this) == (*rhs);
}
