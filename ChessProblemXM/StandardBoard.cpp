#include "stdafx.h"
#include "StandardBoard.h"
#include "StandardPosition.h"


StandardBoard::StandardBoard()
{
	for (char column = 'A'; column <= 'H'; ++column)
	{
		for (unsigned int row = 1; row <= 8; ++row)
		{
			standardPositions[unsigned int(column) - 65][row - 1] = StandardPosition::generateFromChessPosition(column, row).value();
		}
	}
}


optional<shared_ptr<Position>> StandardBoard::getPosition(int column, int row)
{
	if (row < STANDARD_LENGTH && column < STANDARD_LENGTH)
		return standardPositions[column][row];
	else return {};
}

const unsigned int StandardBoard::getPositionIndex(int column, int row)
{
	return column * STANDARD_LENGTH + row;
}

StandardBoard::~StandardBoard()
{
}
