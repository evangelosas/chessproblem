#include "stdafx.h"
#include "StandardPosition.h"

using namespace std;

StandardPosition::StandardPosition(const int c, const int r)
	: column(c),
	  row(r)
{
}

StandardPosition::~StandardPosition()
{
}

int StandardPosition::getRow() const
{
	return row;
}

int StandardPosition::getColumn() const
{
	return column;
}

const string StandardPosition::getName() const
{
	return string(1,static_cast<char>(column + 65)) + to_string(row+1);
}

optional<shared_ptr<Position>> StandardPosition::generateFromChessPosition(const char c, int r)
{
	if (r <= 8)
	{
		int columnNumber = int(c);
		if (columnNumber >= 65 && columnNumber <= 72)
		{
			shared_ptr<StandardPosition> p ( new StandardPosition(columnNumber - 65, r - 1));
			return p;
		}
			
		else if (columnNumber >= 97 && columnNumber <= 104)
		{
			shared_ptr<StandardPosition> p(new StandardPosition(columnNumber - 97, r - 1));
			return p;
		}
	}
	return {};
}



