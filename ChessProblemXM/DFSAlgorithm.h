#pragma once
#include "Algorithm.h"
#include "VisitedItemList.h"

class DFSAlgorithm :
	public Algorithm
{
public:
	DFSAlgorithm();
	DFSAlgorithm(const DFSAlgorithm& that) = delete;
	DFSAlgorithm& operator=(const DFSAlgorithm& that) = delete;
	virtual string solve(shared_ptr<Piece>& piece, const shared_ptr<Position>& ending, unsigned int depth) override;
	virtual ~DFSAlgorithm();

protected:
	virtual string recursiveSolve(shared_ptr<Piece>& piece, const shared_ptr<Position>& ending, unsigned int depth);

private:
	VisitedItemList vpl;
};

